# Bongo Cat changes the weather

## Weather Types
* Sunny
* Clear
* Cloudy
* Overcast
* Rain
* Drizzle
* Snow
* Stormy
* Tornado
* Fog
* Hurrican
* Sandstorm
* Hail
* Wind
* Lightning

## Font
Calibiri 72 px Bold Smooth (disabled Antialiasing)  

## Frames
1. up + none glow
2. down + snow glow
3. up + none glow
4. down + cloudy, rain glow
5. up + none glow
6. down + clear, sunny glow
7. ... (repeat 1)

## GIF
https://gifmaker.me  
Settings:  
Canvas size: 100% 1920 x 1440 px
Animation speed: 500 ms
Repeat times: 0 times

Output: bongo_cat_changes_the_weather.gif  

## License
Bongo Cat changes the weather by waahhhh licensed CC0 1.0  
